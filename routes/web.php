<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware('dateMiddleware')->group(function(){

// });
Route::middleware(['auth', 'verifemail'])->group(function(){
    Route::get('/route-1', 'PesanController@verifemail');
});

Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/route-2', 'PesanController@role');
});

// Route::get('/test', 'TestController@test')->middleware('dateMiddleware');

// Route::get('/admin', 'TestController@admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
