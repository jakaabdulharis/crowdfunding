<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => "admin",
            'email' => "admin@admin.com",
            'password' => Hash::make("123321123"),
            'role_id' => "1",
            'email_verified_at' => date("Y-m-d\TH:i:s"),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
        	'name' => "admin2",
            'email' => "admin2@admin.com",
            'password' => Hash::make("123321123"),
            'role_id' => "1",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
        	'name' => "user",
            'email' => "user@user.com",
            'password' => Hash::make("123321123"),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
        	'name' => "user2",
            'email' => "user2@user.com",
            'password' => Hash::make("123321123"),
            'email_verified_at' => date("Y-m-d\TH:i:s"),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //php artisan db:seed --class=users
    }
}
